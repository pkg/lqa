###################################################################################
# LAVA QA api - This class represents a LAVA test object.
# Copyright (C) 2015 Collabora Ltd.

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################


class Test(object):
    """This class represents a LAVA test"""

    def __init__(self, test, is_v2=True):
        self._is_v2 = is_v2

        if is_v2:
            self._name = test.get('name')
        else:
            self._name = test.get('test_id')

        self._test = test
        self._results = test.get('test_results')
        self._uuid = test.get('analyzer_assigned_uuid')
        self._attributes = test.get('attributes')
        self._attachments = test.get('attachments')
        self._software_ctx = test.get('software_context')
        self._hardware_ctx = test.get('hardware_context')
        self._testdef_metadata = test.get('testdef_metadata')

    def __str__(self):
        if self._is_v2:
            return "{}: {}".format(self._name, self.job)
        else:
            return "{}: {}".format(self._name, self._uuid)

    def is_v2(self):
        return self._is_v2

    @property
    def name(self):
        return self._name

    # LAVA V2 Test properties
    @property
    def result(self):
        return self._test.get('result')

    @property
    def job(self):
        return self._test.get('job')

    @property
    def suite(self):
        return self._test.get('suite')

    @property
    def url(self):
        return self._test.get('url')

    @property
    def logged(self):
        return self._test.get('logged')

    @property
    def unit(self):
        return self._test.get('unit')

    @property
    def duration(self):
        return self._test.get('duration')

    @property
    def timeout(self):
        return self._test.get('timeout')

    @property
    def measurement(self):
        return self._test.get('measurement')

    @property
    def level(self):
        return self._test.get('level')

    @property
    def metadata(self):
        return self._test.get('metadata')

    # LAVA V1 Test properties
    @property
    def uuid(self):
        return self._uuid

    @property
    def results(self):
        return self._results

    @property
    def attributes(self):
        return self._attributes

    @property
    def attachments(self):
        return self._attachments

    @property
    def params(self):
        if self._software_ctx:
            return self._software_ctx['sources'][0]['test_params']

    @property
    def project(self):
        if self._software_ctx:
            return self._software_ctx['sources'][0]['project_name']

    @property
    def image(self):
        if self._software_ctx:
            return self._software_ctx['image']['name']

    @property
    def packages(self):
        if self._software_ctx:
            return self._software_ctx['packages']

    @property
    def branch_url(self):
        if self._software_ctx:
            return self._software_ctx['sources'][0]['branch_url']

    @property
    def branch_rev(self):
        if self._software_ctx:
            return self._software_ctx['sources'][0]['branch_revision']

    @property
    def branch_vcs(self):
        if self._software_ctx:
            return self._software_ctx['sources'][0]['branch_vcs']

    @property
    def testdef_metadata(self):
        return self._testdef_metadata

    @property
    def version(self):
        if self._testdef_metadata:
            return self._testdef_metadata['version']

    @property
    def description(self):
        if self._testdef_metadata:
            return self._testdef_metadata['description']

    @property
    def format(self):
        if self._testdef_metadata:
            return self._testdef_metadata['format']

    @property
    def location(self):
        if self._testdef_metadata:
            return self._testdef_metadata['location']

    @property
    def devices(self):
        if self._hardware_ctx:
            return self._hardware_ctx['devices']

    # Convenient functions to filter tests results.
    @property
    def results_passed(self):
        return self._filter_result('pass')

    @property
    def results_failed(self):
        return self._filter_result('fail')

    @property
    def results_skipped(self):
        return self._filter_result('skip')

    @property
    def results_unknown(self):
        return self._filter_result('unknown')

    def _filter_result(self, result):
        if not self._is_v2:
            return filter(lambda tc: tc['result'] == result, self.results)
