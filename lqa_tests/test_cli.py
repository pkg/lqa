###################################################################################
# LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import sys
import unittest

try:
    from io import StringIO
except ImportError:
    from cStringIO import StringIO
from lqa_tool import Cli
from lqa_tool.settings import settings
from lqa_tool.exceptions import ProfileNotFound

# Decorator required to redirect argparser stderr messages.
def redirect_stderr(func):
    def _r_stderr(obj):
        oldstderr = sys.stderr
        try:
            sys.stderr = StringIO()
            func(obj)
        finally:
            sys.stderr.close()
            sys.stderr = oldstderr
    return _r_stderr

class CliTestCase(unittest.TestCase):

    def setUp(self):
        settings.load_config(config_file='examples/lqa.yaml')
        self.cli = Cli()

class SubmitCliTestCase(CliTestCase):

    def test_cli_submit_requires_no_args(self):
        # Test for the _not_ raising of an exception if no arg passed.
        try:
            self.cli._parse_args(["submit"])
        except SystemExit:
            self.fail("submit command should not require args")

    def test_cli_submit_profile_option_requires_profile(self):
        parsed_args = self.cli._parse_args(["submit", "--profile", "i386"])
        with self.assertRaises(ProfileNotFound):
            parsed_args.func(parsed_args)

    def test_cli_submit_all_profile_option_requires_profile(self):
        parsed_args = self.cli._parse_args(["submit", "--all-profile"])
        with self.assertRaises(ProfileNotFound):
            parsed_args.func(parsed_args)

class CancelCliTestCase(CliTestCase):

    @redirect_stderr
    def test_cli_cancel_requires_args(self):
        with self.assertRaises(SystemExit):
            self.cli._parse_args(["cancel"])

class ResubmitCliTestCase(CliTestCase):

    @redirect_stderr
    def test_cli_resubmit_requires_args(self):
        with self.assertRaises(SystemExit):
            self.cli._parse_args(["resubmit"])

class WaitCliTestCase(CliTestCase):

    @redirect_stderr
    def test_cli_wait_requires_args(self):
        with self.assertRaises(SystemExit):
            self.cli._parse_args(["wait"])

class StatusCliTestCase(CliTestCase):

    @redirect_stderr
    def test_cli_status_requires_args(self):
        with self.assertRaises(SystemExit):
            self.cli._parse_args(["status"])

class ReportCliTestCase(CliTestCase):

    @redirect_stderr
    def test_cli_report_requires_args(self):
        with self.assertRaises(SystemExit):
            self.cli._parse_args(["report"])

class ResultsCliTestCase(CliTestCase):

    @redirect_stderr
    def test_cli_results_requires_args(self):
        with self.assertRaises(SystemExit):
            self.cli._parse_args(["results"])
