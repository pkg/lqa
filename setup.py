#!/usr/bin/env python
###################################################################################
# LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from setuptools import setup, find_packages

from lqa_tool.version import __version__

setup(
    name='lqa',
    version=__version__,
    license='LGPL-2.1+',
    description='LAVA QA command line tool',
    author='Luis Araujo',
    author_email='luis.araujo@collabora.co.uk',
    url='https://gitlab.collabora.com/collabora/lqa',
    packages=find_packages(),
    classifiers = [
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.6',
    ],
    entry_points = {
        'console_scripts': [
            'lqa = lqa_tool:main'
        ]
    },
    include_package_data=False,
    install_requires=['PyYaml', 'jinja2', 'requests'],
    zip_safe=False,
)
